Модуль Users (FRONT) для Yii2
=============================

Модуль пользователей для фронта

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-user-new": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-user-new.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'user' => []
    ],
```
