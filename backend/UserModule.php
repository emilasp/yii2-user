<?php
namespace emilasp\user\backend;

use emilasp\core\CoreModule;

/**
 * Class UserBackendModule
 * @package emilasp\user\backend
 */
class UserModule extends CoreModule
{
    public function init()
    {
        parent::init();
    }
}
