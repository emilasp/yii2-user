<?php
return [
    'Users Issue'       => 'Запросы пользователей',
    'Create User Issue' => 'Создать запрос',
    'User Issues'       => 'Запросы пользователей',
    'User Issue'        => 'Запрос пользователя',
    'Call back phone'        => 'Запрос пользователя',
];
