<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\user\backend\models\Profile */

$this->title = Yii::t('userbackend', 'Create Profile');
$this->params['breadcrumbs'][] = ['label' => Yii::t('userbackend', 'Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
