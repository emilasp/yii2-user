<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\user\backend\models\Service */

$this->title = Yii::t('userbackend', 'Create Service');
$this->params['breadcrumbs'][] = ['label' => Yii::t('userbackend', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
