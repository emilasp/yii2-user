<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\user\backend\models\search\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('userbackend', 'Services');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <p>
        <?= Html::a(Yii::t('userbackend', 'Create Service'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'service',
            'ids',
            'user_id',
            'token',
            // 'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
