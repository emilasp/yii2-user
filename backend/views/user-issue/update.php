<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\user\core\models\UserIssue */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => 'User Issue',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'User Issues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="user-issue-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
