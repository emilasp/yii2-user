<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\user\backend\models\User */

$this->title = Yii::t('userbackend', 'Update {modelClass}: ', [
    'modelClass' => 'User',
]) . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('userbackend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('userbackend', 'Update');
?>
<div class="user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
