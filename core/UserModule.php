<?php
namespace emilasp\user\core;

use emilasp\core\CoreModule;
use emilasp\user\core\events\UserEvent;
use emilasp\user\core\models\User;
use Yii;

/**
 * Class UserModule
 * @package emilasp\user
 */
class UserModule extends CoreModule
{
    public $duration                = 0;
    public $minLengthPassword;
    public $enterAfterRegistration;
    public $socialEnabled           = true;
    public $registrationEnabled     = true;
    public $recoveryPasswordEnabled = true;
    public $loginByUsername         = true;
    public $loginByEmail            = true;
    public $loginByPhone            = true;
    public $routeAfterLogin         = '/';

    /** @var bool используемые layout при логине */
    public $loginLayout;

    /** @var callable|null если не хватает прав */
    public $denyCallback;

    /** @var string Используется для генерации auth_key в Identity */
    public $key_auth = 'test';

    public $eventClass = '\emilasp\user\core\events\UserEvent';

    public function init()
    {
        parent::init();
    }

    /**
     * Привязываем роль к пользователю
     *
     * @param integer $role
     * @param /emilasp/front/user/models/FrontUser $user
     */
    public function addRole($role, $user)
    {
        $auth      = \Yii::$app->authManager;
        $adminRole = $auth->getRole($role);
        $auth->assign($adminRole, $user->id);
    }

    /** Авторизуем пользователя
     *
     * @param User $user
     * @param bool $rememberMe
     *
     * @return bool
     */
    public function login(User $user, $rememberMe = true)
    {
        $login = Yii::$app->user->login($user, ($rememberMe ? $this->duration : 0));

        if ($login) {
            $auth     = Yii::$app->authManager;
            $roleUser = $auth->getRole($user->role);
            if ($roleUser !== null) {
                $auth->assign($roleUser, $user->id);
                return true;
            }
        }
        return false;
    }
}
