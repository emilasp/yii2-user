<?php
namespace emilasp\user\core\authclient;

use emilasp\user\core\models\Profile;

/**
 * Class ClientHelper
 * @package emilasp\user\core\authclient
 */
class ClientHelper
{
    /** @var array аттрибуты для записи */
    public static $defaultData = [
        'id' => null,
        'email' => null,
        'username' => null,
        'firstname' => null,
        'lastname' => null,
        'url' => null,
        'photo' => null,
        'gender' => null,
        'city' => null,
    ];

    /** Форматируе пришедшие с сервисов атрибуты
     * @param array $data
     * @param array $dataTemplate
     * @return array
     */
    public static function clientDataAdapter($data, $dataTemplate)
    {
        $returnData = self::$defaultData;
        foreach ($dataTemplate as $name => $param) {
            if (is_callable($param) && $param != 'link') {
                $returnData[$name] = $param();
            } else {
                if (isset($data[$param])) {
                    $returnData[$name] = $data[$param];
                }
            }
        }
        return $returnData;
    }

    /** Парсим ФИО
     */
    private static function getNames($username)
    {
        if (strpos($username, ' ')) {
            $arrNames = explode(' ', $username);
            return [$arrNames[0], str_replace($arrNames[0] . ' ', '', $username)];
        }
        return [$username, ''];
    }

    /** Парсим пол
     * @param string $value
     * @return int
     */
    public static function parseGender($value)
    {
        $male = ['m', 'male', '1'];
        $female = ['f', 'female', '1'];

        if (in_array($value, $male)) {
            return Profile::GENDER_MALE;
        } elseif (in_array($value, $female)) {
            return Profile::GENDER_FEMALE;
        }
        return Profile::GENDER_MALE;
    }
}