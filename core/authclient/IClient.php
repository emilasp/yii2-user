<?php
namespace emilasp\user\core\authclient;

/**
 * Interface IClient
 * @package emilasp\user\core\authclient
 */
interface IClient
{
    public function getDataTemplate();
    public function getData();
}