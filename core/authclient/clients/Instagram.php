<?php
namespace emilasp\user\core\authclient\clients;

use emilasp\user\core\authclient\ClientHelper;
use emilasp\user\core\authclient\IClient;
use yii\authclient\OAuth2;

/**
 * Class Instagram
 * @package emilasp\user\core\authclient\clients
 */
class Instagram extends OAuth2 implements IClient
{
    /**
     * @inheritdoc
     */
    public $authUrl = 'https://api.instagram.com/oauth/authorize';
    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://api.instagram.com/oauth/access_token';
    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'https://api.instagram.com/v1';
    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
        $response = $this->api('users/self', 'GET');
        return $response['data'];
    }
    /**
     * @inheritdoc
     */
    protected function apiInternal($accessToken, $url, $method, array $params, array $headers)
    {
        return $this->sendRequest($method, $url . '?access_token=' . $accessToken->getToken(), $params, $headers);
    }
    /**
     * @inheritdoc
     */
    protected function defaultName()
    {
        return 'instagram';
    }
    /**
     * @inheritdoc
     */
    protected function defaultTitle()
    {
        return 'Instagram';
    }


    /**
     * @return array
     */
    public function getDataTemplate()
    {
        return [
            'id' => 'id',
            'username' => 'name',
            'firstname' => 'name',
            'lastname' => 'username',
            'gender' => 'gender',
            'photo' => 'profile_image_url',
            'url' => 'url',
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ClientHelper::clientDataAdapter($this->userAttributes, $this->getDataTemplate());
    }
}