<?php
namespace emilasp\user\core\authclient\clients;

use emilasp\user\core\authclient\ClientHelper;
use emilasp\user\core\authclient\IClient;
use yii\authclient\OAuth2;

/**
 * Class Odnoklassniki
 * @package emilasp\user\core\authclient\clients
 */
class Odnoklassniki extends OAuth2 implements IClient
{
    /**
     * @var string
     */
    public $applicationKey;
    /**
     * @inheritdoc
     */
    public $authUrl = 'http://www.odnoklassniki.ru/oauth/authorize';
    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://api.odnoklassniki.ru/oauth/token.do';
    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'http://api.odnoklassniki.ru';
    /**
     * @inheritdoc
     */
    public $scope = 'VALUABLE_ACCESS';

    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
        return $this->api('api/users/getCurrentUser', 'GET');
    }

    /**
     * @inheritdoc
     */
    protected function apiInternal($accessToken, $url, $method, array $params, array $headers)
    {
        $params['access_token']    = $accessToken->getToken();
        $params['application_key'] = $this->applicationKey;
        $params['method']          = str_replace('/', '.', str_replace('api/', '', $url));
        $first                     = 'application_key=' . $this->applicationKey . 'method=' . $params['method'];
        $second                    = md5($params['access_token'] . $this->clientSecret);
        $params['sig']             = md5($first . $second);
        return $this->sendRequest($method, $url, $params, $headers);
    }

    /**
     * @inheritdoc
     */
    protected function defaultName()
    {
        return 'odnoklassniki';
    }

    /**
     * @inheritdoc
     */
    protected function defaultTitle()
    {
        return 'Odnoklassniki';
    }

    /**
     * @return array
     */
    public function getDataTemplate()
    {
        return [
            'id'        => 'id',
            'username'  => 'name',
            'firstname' => 'name',
            'lastname'  => 'username',
            'gender'    => 'gender',
            'photo'     => 'profile_image_url',
            'url'       => 'url',
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ClientHelper::clientDataAdapter($this->userAttributes, $this->getDataTemplate());
    }
}
