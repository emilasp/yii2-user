<?php
namespace emilasp\user\core\authclient\clients;

use emilasp\user\core\authclient\ClientHelper;
use emilasp\user\core\authclient\IClient;

/**
 * Class ClientHelper
 * @package emilasp\user\core\authclient\clients
 */
class Twitter extends \yii\authclient\clients\Twitter implements IClient
{

    public function getDataTemplate()
    {
        return [
            'id' => 'id',
            'username' => 'name',
            'firstname' => 'name',
            'lastname' => 'username',
            'gender' => 'gender',
            'photo' => 'profile_image_url',
            'url' => 'url',
        ];
    }

    public function getData()
    {
        return ClientHelper::clientDataAdapter($this->userAttributes, $this->getDataTemplate());
    }

}