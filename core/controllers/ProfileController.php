<?php
namespace emilasp\user\core\controllers;

use Yii;
use emilasp\core\components\base\Controller;
use emilasp\user\core\models\User;
use emilasp\user\core\models\Profile;
use \emilasp\user\core\models\search\FrontProfileSearch;
use yii\filters\AccessControl;

/**
 * PageController implements the CRUD actions for STool model.
 */
class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->setSort([
            'attributes' => [
                'email' => [
                    'asc' => ['name' => SORT_DESC],
                    'desc' => ['name' => SORT_ASC],
                    'label' => Yii::t('site', 'Name')
                ],
                'first_name' => [
                    'asc' => ['first_name' => SORT_DESC],
                    'desc' => ['first_name' => SORT_ASC],
                    'label' => Yii::t('site', 'Datecreate')
                ],
                'rate' => [
                    'asc' => ['rate' => SORT_ASC],
                    'desc' => ['rate' => SORT_DESC],
                    'label' => Yii::t('site', 'Rate')
                ],
                'count_open' => [
                    'asc' => ['count_open' => SORT_ASC],
                    'desc' => ['count_open' => SORT_DESC],
                    'label' => Yii::t('site', 'Count Open')
                ],
            ]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}
