<?php
namespace emilasp\user\core\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\log\Logger;
use emilasp\core\components\base\Controller;
use emilasp\user\core\models\forms\RecoveryForm;
use emilasp\user\core\models\forms\SendauthForm;
use emilasp\user\core\models\User;

/**
 * SOrderController implements the CRUD actions for STool model.
 */
class RecoveryController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['user', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow'        => false,
                        'denyCallback' => [$this, 'goHome'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionUser()
    {
        $model = new SendauthForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::find()->where(['email' => $model->email])->with('profiles')->one();

            if ($user) {
                $auth = $user->generateAuthRoEmail();
                $url  = Url::toRoute('/user/recovery/recovery', true) . '/?email=' . $user->email . '&code=' . $auth;
                Yii::$app->core->sendMail(
                    Yii::t("user", "Recovery password email subject"),
                    [$user->email],
                    false,
                    'users/recoveryLink',
                    ['userName' => $user->profiles->first_name, 'link' => $url]
                );
            }

            return $this->render('linkSend');
        }

        return $this->render('user', ['model' => $model]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = new RecoveryForm();

        $model->email = Yii::$app->request->get('email', false);
        $model->code  = Yii::$app->request->get('code', false);

        $user = User::find()->where(['email' => $model->email])->with('profiles')->one();

        /*if (is_null($user) || !$model->email || !$model->code || !$user->validateAuthRoEmail($model->code)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }*/

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->password = $user->generatePassword($model->password);

            if ($user->save()) {
                Yii::$app->core->sendMail(
                    Yii::t("user", "Recovery password congratulation email subject"),
                    [$model->email],
                    false,
                    'users/congrRecovery',
                    ['userName' => $user->profiles->first_name]
                );
                Yii::$app->core->alert(Yii::t("user", "Password recovery success"), CoreComponent::ALERT_SUCCESS);
                $this->goHome();
            } else {
                Yii::$app->core->addLog(
                    Yii::t('user', 'Password recovery error') . '(' . VarDumper::dump($user->getErrors(), 10, 1) . ')',
                    true,
                    Logger::LEVEL_ERROR
                );
            }
        }
        return $this->render('index', ['model' => $model]);
    }
}
