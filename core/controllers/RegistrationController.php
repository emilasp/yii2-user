<?php
namespace emilasp\user\core\controllers;

use Yii;
use yii\filters\AccessControl;
use emilasp\user\core\models\Profile;
use emilasp\core\components\base\Controller;
use emilasp\user\core\models\forms\RegistrationForm;

/**
 * RegistrationController implements the CRUD actions for STool model.
 */
class RegistrationController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['user', 'captcha'],
                'rules' => [
                    [
                        'actions' => ['captcha'],
                        'allow'   => true,
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'actions' => ['user'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                ],
            ],
        ];
    }


    /**
     * @return string|\yii\web\Response
     */
    public function actionUser()
    {
        if (Yii::$app->getModule('user')->registrationEnabled) {
            $this->redirect('/');
        }

        $model = new RegistrationForm();
        if ($model->load($_POST) && $model->save()) {
            $profile             = new Profile();
            $profile->user_id    = $model->id;
            $profile->first_name = $model->first_name;
            $profile->city       = $model->city_id;
            $profile->status     = $model->status;
            $profile->save();

            Yii::$app->core->sendMail(
                Yii::t('user', 'Registration Fine Email subject'),
                [$model->email],
                false,
                'users/afterRegistration',
                ['userName' => $model->first_name]
            );

            //@TODO Дописать вход после регистрации
            if (Yii::$app->getModule('user')->enterAfterRegistration) {
                /*$loginForm = new LoginForm();

                $loginForm->username = '';
                $loginForm->password = '';
                if ($loginForm->validate() && $loginForm->login()) {
                    return $this->goBack();
                }*/
            }

            return $this->redirect('/');
        } else {
            return $this->renderAjax(
                '@vendor/emilasp/yii2-front-site/extensions/topmenu/views/_registration',
                ['model' => $model]
            );
        }
    }
}
