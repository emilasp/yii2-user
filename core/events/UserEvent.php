<?php
namespace emilasp\user\core\events;

use emilasp\user\core\models\User;
use Yii;
use yii\base\Event;

/**
 * Class UserEvent
 * @package emilasp\user\core\events
 */
class UserEvent extends Event
{
    const EVENT_NEW_USER = 'onNewUser';
    const EVENT_CALL_USER = 'onCallUser';
    const EVENT_MESSAGE_USER = 'onMessageUser';

    /** События
     * @var array
     */
    public static $events = [
        self::EVENT_NEW_USER,
        self::EVENT_CALL_USER,
        self::EVENT_MESSAGE_USER,
    ];
    
    /** @var  User */
    public $user;
    
    /** @var  array */
    public $params = [];
    
    
    /** Событие создания нового пользователя
     * @param UserEvent $event
     */
    public function onNewUser(UserEvent $event)
    {
        if ($event->user && $event->user->email) {
            Yii::$app->sender->sendMail([$event->user->email], Yii::t('user', 'User Email: Registration'),
                'users/registration-finish',
                ['username' => $event->user->email, 'password' => $event->params['password']]);
        }
    }

    /** Событие пользователь отправил сообщение
     * @param UserEvent $event
     */
    public function onMessageUser(UserEvent $event)
    {
        if (($event->user && $event->user->email) || $event->params['issue']->email) {
            $email = $event->params['issue']->email;

            if (!$email && $event->user && $event->user->email) {
                $email = $event->user->email;
            }

            Yii::$app->sender->sendMail(
                [$email],
                Yii::t('user', 'User Email: user sand message'),
                'users/message-user',
                ['issue' => $event->params['issue']]
            );
        }

        /** Send admin email */
        Yii::$app->sender->sendMail(
            [Yii::$app->sender->email['admin'], Yii::$app->sender->email['support']],
            Yii::t('user', 'User Email: Admin user sand message'),
            'users/message-admin',
            ['issue' => $event->params['issue']]
        );

        Yii::$app->sender->sendSms(
            Yii::$app->params['adminPhone'],
            'User send message: ' . $event->params['issue']->id
        );
    }

    /** Событие пользователь отправил сообщение
     * @param UserEvent $event
     */
    public function onCallUser(UserEvent $event)
    {
        /** Send admin email */
        Yii::$app->sender->sendMail(
            [Yii::$app->sender->email['admin'], Yii::$app->sender->email['support']],
            Yii::t('user', 'User Email: Admin user call message'),
            'users/call-admin',
            ['issue' => $event->params['issue']]
        );

        Yii::$app->sender->sendSms(
            Yii::$app->params['adminPhone'],
            'User send recall: ' . $event->params['issue']->phone
        );
    }
}