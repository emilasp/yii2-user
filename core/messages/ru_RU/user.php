<?php
return [
    'Registration'                                   => 'Зарегистрироваться',
    'Recovery password'                              => 'Восстановление пароля',
    'Recovery password email subject'                => 'Восстановление пароля',
    'Recovery password congratulation email subject' => 'Вы восстановили пароль',
    'Password recovery success'                      => 'Пароль успешно изменён',
    'Password recovery error'                        => 'Ошибка восстановления пароля',
    'Registration Fine Email subject'                => 'Поздравляем в регистрацией!',
    'Anonim'                                         => 'Аноним',

    'ID'         => 'ID',
    'Password'   => 'Пароль',
    'Auth Key'   => 'Auth Key',
    'Role'       => 'Роль',
    'City'       => 'Город',
    'Created At' => 'Дата создания',
    'Updated At' => 'Дата изменения',
    'Status'     => 'Статус',
    'Email'      => 'email',

    'User ID'    => 'Пользователь',
    'First Name' => 'Имя',
    'Last Name'  => 'Фамилия',
    'Hometown'   => 'Область',
    'Gender'     => 'Пол',
    'Photo'      => 'Фото',
    'Url'        => 'Страница',

    'Email not found'     => 'Не верный email',
    'Incorrect password'  => 'Не верный пароль',
    'Remember Me'         => 'Запомнить',
    'Confirm Password'    => 'Подтверждение пароля',
    'Registration button' => 'Зарегистрироваться',
    'User Profile'        => 'Профиль',


    'User Email: Registration'                       => 'Регистрация в интернет магазине',
    'Login'                                          => 'Логин',
    'Please fill out the following fields to login:' => 'Пожалуйста заполните все поля для авторизации',

    'Issue message'                   => 'Отправить сообщение администратору',
    'Issue help text message'         => 'Вы отправляете сообщение администратору',
    'Issue message product'           => 'Задать вопрос по товару',
    'Issue product help text message' => 'Вы отправляете вопрос по товару',
    'Issue recall'                    => 'Бесплатный звонок с сайта',
    'Issue call help text message'    => 'В ближайшее время мы вам перезвоним',

    'Recall'          => 'Заказать звонок',
    'Send message'    => 'Отправить сообщение администратору',
    'Message success' => 'Ваш запрос отправлен',

    'User Email: user sand message'       => 'Вы отправили сообщение',
    'User Email: Admin user sand message' => 'Пользователь отправил сообщение',
    'User Email: Admin user call message' => 'Пользователь оставил заявку на обратный звонок',
];
