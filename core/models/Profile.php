<?php
namespace emilasp\user\core\models;

use emilasp\files\behaviors\FileBehavior;
use Yii;
use emilasp\core\behaviors\ImageBehavior;
use \emilasp\core\components\base\ActiveRecord;

/**
 * This is the model class for table "front_user_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $hash
 * @property string $name
 * @property string $firstname
 * @property string $lastname
 * @property string $hometown
 * @property integer $gender
 * @property string $photo
 * @property string $url
 * @property float $rate
 * @property string $data
 */
class Profile extends ActiveRecord
{
    const GENDER_MALE   = 1;
    const GENDER_FEMALE = 0;

    public function behaviors()
    {
        return [
            [   // Image save Behavior
                'class'     => FileBehavior::className(),
                'attribute' => 'photo',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'hash'], 'required'],
            [['user_id', 'gender'], 'integer'],
            [['rate'], 'number'],
            [['data'], 'safe'],
            [['hash'], 'string', 'max' => 32, 'min' => 32],
            [['firstname', 'lastname', 'name'], 'string', 'max' => 50],
            [['hometown', 'photo', 'url'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('user', 'ID'),
            'user_id'   => Yii::t('user', 'User ID'),
            'hash'      => Yii::t('user', 'Hash profile'),
            'name'      => Yii::t('user', 'First Name'),
            'firstname' => Yii::t('user', 'First Name'),
            'lastname'  => Yii::t('user', 'Last Name'),
            'email'     => Yii::t('user', 'Email'),
            'hometown'  => Yii::t('user', 'Hometown'),
            'gender'    => Yii::t('user', 'Gender'),
            'photo'     => Yii::t('user', 'Photo'),
            'url'       => Yii::t('user', 'Url'),
            'rate'      => Yii::t('user', 'Rate'),
            'data'      => Yii::t('user', 'Data Json'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            return true;
        }
        return false;
    }

    public function getFullName()
    {
        if ($this->firstname == $this->lastname) {
            return $this->firstname;
        }
        return $this->firstname . ' ' . $this->lastname;
    }
}
