<?php
namespace emilasp\user\core\models;

use Yii;
use emilasp\user\models\identity\IdentityServices;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "front_user_services".
 *
 * @property integer $id
 * @property string $service
 * @property string $ids
 * @property integer $user_id
 * @property string $token
 * @property string $link
 */
class Service extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service', 'ids', 'user_id', 'token'], 'required'],
            [['user_id'], 'integer'],
            [['service'], 'string', 'max' => 28],
            [['ids'], 'string', 'max' => 64],
            [['token'], 'string', 'max' => 100],
            [['link'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'service' => Yii::t('user', 'Service'),
            'ids' => Yii::t('user', 'Ids'),
            'user_id' => Yii::t('user', 'User ID'),
            'token' => Yii::t('user', 'token'),
            'link' => Yii::t('user', 'Link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(IdentityServices::className(), ['id' => 'user_id'])->with('profiles');
    }
}
