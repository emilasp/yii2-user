<?php
namespace emilasp\user\core\models;

use emilasp\geoapp\models\GeoKladr;
use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\models\Setting;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "front_user_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $phone
 * @property string $role
 * @property string $password
 * @property string $auth_key
 * @property string $status
 * @property integer $city_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class User extends \emilasp\core\components\base\ActiveRecord implements IdentityInterface
{
    const STATUS_VERIFY = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETE = 2;
    const STATUS_BAN    = 3;

    public $authKey;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_user';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user) || Yii::$app->user->isGuest) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'role', 'status', 'auth_key'], 'required'],
            [['status', 'city_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['password', 'auth_key'], 'string', 'max' => 128],
            [['role'], 'string', 'max' => 10],
            [['username'], 'string', 'min' => 3, 'max' => 50],
            [['phone'], 'string', 'min' => 10, 'max' => 10],
            [['email'], 'string', 'max' => 128],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('user', 'ID'),
            'username'   => Yii::t('user', 'Username'),
            'email'      => Yii::t('user', 'Email'),
            'phone'      => Yii::t('user', 'Phone'),
            'password'   => Yii::t('user', 'Password'),
            'auth_key'   => Yii::t('user', 'Auth Key'),
            'role'       => Yii::t('user', 'Role'),
            'city_id'    => Yii::t('user', 'City'),
            'status'     => Yii::t('user', 'Status'),
            'created_at' => Yii::t('user', 'Datecreate'),
            'updated_at' => Yii::t('user', 'Dateupdate'),
            'created_by' => Yii::t('user', 'Owner'),
            'updated_by' => Yii::t('user', 'Updated'),
        ];
    }

    /**
     * Проверяем пароль
     *
     * @param User $user
     * @param string $password
     *
     * @return bool
     */
    public function verifyPassword($user, $password)
    {
        return Yii::$app->security->validatePassword($password, $user->password);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        return $this->generateAuthKey();
    }

    /**
     * @param string $authKey
     *
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * @param int|string $id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findIdentity($id)
    {
        return self::getDb()->cache(function () use ($id) {
            return self::findOne(['id' => $id]);
        }, 300);
    }

    /**
     * @param $username
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['email' => $username])->one();
    }

    /**
     * @param $newPassword
     *
     * @return string
     */
    public function generatePassword($newPassword)
    {
        return Yii::$app->security->generatePasswordHash($newPassword);
    }

    /**
     * @return string
     */
    public function generateAuthKey()
    {
        return md5($this->email . Yii::$app->getModule('user')->key_auth);
    }

    /**
     * Finds an identity by the given secrete token.
     *
     * @param string $token the secrete token
     * @param null $type type of $token
     *
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(GeoKladr::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['created_by' => 'id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!$this->password) {
                $this->password = $this->getOldAttribute('password');
            } elseif (!$insert && $this->password !== $this->getOldAttribute('password')) {
                $this->password = Yii::$app->security->generatePasswordHash($this->password);
            }
            return true;
        }
        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }
}
