<?php

namespace emilasp\user\core\models;

use emilasp\core\validators\PhoneValidator;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users_issue".
 *
 * @property integer $id
 * @property integer $type
 * @property string $title
 * @property string $text
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $info
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class UserIssue extends \emilasp\core\components\base\ActiveRecord
{
    const TYPE_RECALL  = 1;
    const TYPE_MESSAGE = 2;
    const TYPE_PRODUCT = 3;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'user_issue_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'user_issue_type',
            ],
            'variety_status'  => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_issue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['text'],
                'required',
                'when' => function ($model) {
                    return $model->type != self::TYPE_RECALL;
                },
            ],

            [
                'phone',
                'required',
                'when' => function ($model) {
                    return $model->type == self::TYPE_RECALL || $model->type == self::TYPE_PRODUCT;
                },
            ],
            [
                'email',
                'required',
                'when' => function ($model) {
                    return $model->type == self::TYPE_MESSAGE;
                },
            ],

            [['type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['name', 'phone', 'email'], 'string', 'max' => 50],
            [['info'], 'string', 'max' => 250],
            [['email'], 'email'],

            [['phone'], PhoneValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'type'       => Yii::t('site', 'Type'),
            'title'      => Yii::t('site', 'Title'),
            'text'       => Yii::t('site', 'Text'),
            'name'       => Yii::t('site', 'User Name'),
            'phone'      => Yii::t('site', 'Phone'),
            'email'      => Yii::t('site', 'Email'),
            'info'       => Yii::t('user', 'Info'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }
}
