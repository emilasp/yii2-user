<?php
namespace emilasp\user\core\models\forms;

use Yii;
use yii\base\Model;
use emilasp\user\core\models\User;
use emilasp\user\core\UserModule;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    /**
     * @var string Username and/or email
     */
    public $username;

    /**
     * @var string Password
     */
    public $password;

    /**
     * @var bool If true, users will be logged in for $loginDuration
     */
    public $rememberMe = true;

    /** @var  UserModule */
    private $userModule;

    /** @var User|null */
    private $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['username', 'validateUser'],
            ['password', 'validatePassword'],
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * Init
     */
    public function init()
    {
        $this->userModule = Yii::$app->getModule('user');
    }

    /*/**
     * Validate user
     */
    public function validateUser()
    {
        // check for valid user
        $user = $this->getUser();
        if (!$user) {
            $this->addError('username', Yii::t('user', 'User not found'));
        }
    }

    /**
     * Validate password
     */
    public function validatePassword()
    {
        // skip if there are already errors
        if ($this->hasErrors()) {
            return;
        }

        // check password
        /** @var \emilasp\user\core\models\identity\Identity $user */
        $user = $this->getUser();
        if (!$user->verifyPassword($user, $this->password)) {
            $this->addError('password', Yii::t('user', 'Incorrect password'));
        }
    }

    /**
     * Get user based on email and/or username
     *
     * @return \emilasp\user\core\models\identity\Identity|null
     */
    public function getUser()
    {
        if (!$this->user) {
            $queryCond = ['or'];

            if ($this->userModule->loginByUsername) {
                $queryCond[] = ['username' => $this->username];
            }
            if ($this->userModule->loginByEmail) {
                $queryCond[] = ['email' => $this->username];
            }
            if ($this->userModule->loginByPhone) {
                $queryCond[] = ['phone' => $this->username];
            }

            if (count($queryCond) > 1) {
                $this->user = User::find()
                                  ->where($queryCond)
                                  ->one();
            }
        }

        return $this->user;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'   => $this->getUsernameLabel(),
            'password'   => Yii::t('user', 'Password'),
            'rememberMe' => Yii::t('user', 'Remember Me'),
        ];
    }

    /** Получаем label для поля username eв зависимости от настроек модуля
     * @return string
     */
    private function getUsernameLabel()
    {
        $username = [];
        if ($this->userModule->loginByUsername) {
            $username[] = Yii::t('user', 'Username');
        }
        if ($this->userModule->loginByEmail) {
            $username[] = Yii::t('user', 'Email');
        }
        if ($this->userModule->loginByPhone) {
            $username[] = Yii::t('user', 'Phone');
        }
        return implode('/', $username);
    }

    /**
     * Validate and log user in
     *
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            $user  = $this->getUser();
            return Yii::$app->getModule('user')->login($user, $this->rememberMe);
        }
        $tt = $this->getErrors();
        return false;
    }
}
