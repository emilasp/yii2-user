<?php
namespace emilasp\user\core\models\forms;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class RecoveryForm extends Model
{
    /**
     * @var string Username and/or email
     */
    public $password;
    public $repassword;
    public $email;
    public $code;
    public $captcha;

    protected $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [["repassword", "password", "email", "code"], "required"],//, "passwordConfirm", "city_id"
            [['password'], 'string', 'min' => Yii::$app->getModule("user")->minLengthPassword],
            [['password'], 'filter', 'filter' => 'trim'],
            [['repassword'], 'compare', 'compareAttribute' => 'password', 'message' => 'Passwords do not match'],
            [["email", "code"], 'safe'],
            ['captcha', 'required'],
            ['captcha', 'captcha', 'captchaAction' => '/user/registration/captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            "password" => Yii::t('user', "Password"),
            "repassword" => Yii::t('user', "Confirm Password"),
        ];
    }
}
