<?php
namespace emilasp\user\core\models\forms;

use Yii;

/**
 * LoginForm is the model behind the login form.
 */
class RegistrationForm extends \emilasp\user\core\models\identity\Identity
{
    public $passwordConfirm;
    public $first_name;
    public $captcha;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [["email", "password", 'first_name'], "required"],
            //, "passwordConfirm", "city_id"
            [['first_name', "email"], 'string', 'max' => 50],
            ["email", "email"],
            [['email'], 'unique'],
            [['email'], 'filter', 'filter' => 'trim'],
            [['password'], 'string', 'min' => Yii::$app->getModule("user")->minLengthPassword],
            [['password'], 'filter', 'filter' => 'trim'],
            //[['passwordConfirm'], 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('user','Passwords do not match')],
            [['city_id'], 'integer'],
            ['role', 'default', 'value' => \emilasp\user\core\rbac\PhpManager::ROLE_USER],
            ['status', 'default', 'value' => self::STATUS_VALIDATE],
            ['captcha', 'required'],
            ['captcha', 'captcha', 'captchaAction' => '/user/registration/captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            "email"           => Yii::t("user", "Email"),
            "password"        => Yii::t("user", "Password"),
            "passwordConfirm" => Yii::t("user", "Confirm Password"),
            "first_name"      => Yii::t("user", "First Name"),
            "city_id"         => Yii::t("user", "City"),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            return true;
        }
        return false;
    }
}
