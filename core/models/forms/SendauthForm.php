<?php
namespace emilasp\user\core\models\forms;

use Yii;
use yii\base\Model;
use emilasp\front\user\models\FrontUser;
use emilasp\front\user\models\identity\Identity;

/**
 * LoginForm is the model behind the login form.
 */
class SendauthForm extends Model
{
    /**
     * @var string Username and/or email
     */
    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
            ['email', 'validateUser'],
        ];
    }

    /*/**
     * Validate user
     */
    public function validateUser()
    {
        // check for valid user
        $user = FrontUser::find()->where(['email' => $this->email])->one();
        if (!$user) {
            $this->addError("email", Yii::t("user", "Email not found"));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            "email" => Yii::t("user", "Email"),
        ];
    }
}
