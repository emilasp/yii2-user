<?php
namespace emilasp\user\core\models\identity;

use Yii;
use emilasp\user\core\rbac\PhpManager;
use emilasp\user\core\models\User;

/**
 * Class Identity
 * @package emilasp\user\core\models\identity
 */
class Identity extends User
{
    //public $username;
    public $newPassword;

    /**
     * @var array EAuth attributes
     */
    //public $profile;

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->newPassword) {
            $this->password = $this->generatePassword($this->newPassword);
        }
        if (!$this->role) {
            $this->role = PhpManager::ROLE_USER;
        }

        $this->auth_key = $this->getAuthKey();

        return parent::beforeSave($insert);
    }
}
