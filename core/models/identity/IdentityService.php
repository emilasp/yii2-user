<?php
namespace emilasp\user\core\models\identity;

use Yii;
use emilasp\user\core\models\Service;
use yii\helpers\Json;
use emilasp\user\core\models\User;
use emilasp\user\core\models\Profile;
use emilasp\user\core\rbac\PhpManager;

/**
 * Class IdentityService
 * @package emilasp\user\core\models\identity
 */
class IdentityService extends Service
{
    //public $username;

    /**
     * @var array EAuth attributes
     */
    //public $profile;

    /** Получаем пользователя по атрибутам из соц сервисов
     *
     * @param string $service
     * @param string $id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findIdentityByAuth($service, $id)
    {
        return self::find()->where(['service' => $service, 'ids' => $id])->one();
    }

    /** Авторизуем
     * Создаём пользователя, если его нет в БД
     * И обновляем профиль если он есть и отличается хеш
     *
     * @param $service
     *
     * @return bool
     */
    public function auth($service)
    {
        $data = $service->getData();

        $serviceModel = self::find()->where(['service' => $service->id, 'ids' => $data['id']])->one();


        if (!$serviceModel) {
            $user = $this->createUser($service->id, $data);
            if ((
                $user
                && $this->createProfile($user->id, $data)
                && $this->createService($service->id, $user->id, $data)
            )) {
                Yii::$app->getModule('user')->login($user);
                return true;

            }
        } else {
            $user = User::findOne($serviceModel->user_id);
            $this->updateProfile($user->id, $data);
            Yii::$app->getModule('user')->login($user);
            return true;
        }
        return false;
    }

    /** Создаём пользователя
     *
     * @param string $service
     *
     * @return bool|User
     */
    private function createUser($service)
    {
        $user           = new Identity();
        $user->city_id  = 1; //TODO Прикручивать город из БД
        $user->username = $service;
        $user->email    = $service;
        $user->auth_key = $user->generateAuthKey();
        $user->password = $user->generatePassword($user->auth_key);
        $user->role     = PhpManager::ROLE_USER;
        $user->status   = User::STATUS_ACTIVE;
        if ($user->save()) {
            return $user;
        }
        return false;
    }

    /** Создаём профиль
     *
     * @param integer $userId
     * @param array $data
     *
     * @return bool|Profile
     */
    private function createProfile($userId, $data)
    {
        $profile          = new Profile();
        $profile->user_id = $userId;
        $profile->hash    = self::getHashProfile($data);
        $profile->setAttributes($data);
        if ($profile->save()) {
            return $profile;
        }
        return false;
    }

    /** Создаём сервис
     *
     * @param string $serviceId
     * @param integer $userId
     * @param array $data
     *
     * @return bool|Service
     */
    private function createService($serviceId, $userId, $data)
    {
        $service          = new Service();//'service', 'ids', 'user_id', 'token'
        $service->service = $serviceId;
        $service->ids     = (string)$data['id'];
        $service->user_id = $userId;
        $service->token   = $serviceId; //TODO Получать токен
        if ($service->save()) {
            return $service;
        }
        return false;
    }

    /** Проверяем и обновляем профиль, если данные изменились
     *
     * @param integer $userId
     * @param array $data
     *
     * @return bool|Profile
     */
    private function updateProfile($userId, $data)
    {
        $profile = Profile::findOne(['user_id' => $userId]);

        $hash = self::getHashProfile($data);

        if ($profile->hash !== $hash) {
            $profile->hash = self::getHashProfile($data);
            $profile->setAttributes($data);
            if ($profile->save()) {
                return $profile;
            }
        }
        return false;
    }

    /** Хешируем параметры пользователя из сервиса
     *
     * @param array $data
     *
     * @return string
     */
    public static function getHashProfile($data)
    {
        return md5(Json::encode($data));
    }
}
