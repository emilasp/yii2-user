<?php
namespace emilasp\user\core\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\user\core\models\Profile;

/**
 * ProfileSearch represents the model behind the search form about `emilasp\site\models\Profile`.
 */
class ProfileSearch extends Profile
{
    public function rules()
    {
        return [
            [['user_id', 'role', 'status', 'city_id'], 'integer'],
            [['email'], 'string', 'max' => 128],
            [['first_name', 'last_name'], 'string', 'max' => 50],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Profile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->role,
            'role' => $this->status,
            'status' => $this->status,
            'city_id' => $this->status,
            'count_open' => $this->status,
            'rate' => $this->status,
            //'count_open' => $this->count_open,
            //'rate' => $this->rate,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'first_name', $this->first_name]);
        $query->andFilterWhere(['like', 'last_name', $this->last_name]);

        return $dataProvider;
    }
}
