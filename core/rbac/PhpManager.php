<?php
namespace emilasp\user\core\rbac;

use Yii;
use yii\helpers\Json;
use yii\rbac\Assignment;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\Role;

/**
 * Class PhpManager
 * @package emilasp\front\user\rbac
 */
class PhpManager extends \yii\rbac\PhpManager
{
    const ROLE_GUEST   = 'guest';
    const ROLE_USER    = 'user';
    const ROLE_ADMIN   = 'admin';

    public function init()
    {
        parent::init();
        // Create roles
    }

    /**
     * Saves assignments data into persistent storage.
     */
    public function getAssignmentsCache()
    {
        $session    = Yii::$app->session;
        $assignment = $session->get('assignment', '{}');

        return \yii\helpers\Json::decode($assignment);
    }

    /**
     * Saves assignments data into persistent storage.
     */
    protected function saveAssignments()
    {
        $assignmentData = [];
        foreach ($this->assignments as $userId => $assignments) {
            foreach ($assignments as $assignment) {
                /* @var $assignment Assignment */
                $assignmentData[$userId][] = $assignment->roleName;
            }
        }

        $session = Yii::$app->session;
        $session->set('assignment', Json::encode($assignmentData));
        //$this->saveToFile($assignmentData, $this->assignmentFile);
    }

    /**
     * Loads authorization data from persistent storage.
     */
    protected function load()
    {
        $this->children    = [];
        $this->rules       = [];
        $this->assignments = [];
        $this->items       = [];

        $items      = $this->loadFromFile($this->itemFile);
        $itemsMtime = @filemtime($this->itemFile);

        $assignments      = $this->getAssignmentsCache();
        $assignmentsMtime = time();

        $rules = $this->loadFromFile($this->ruleFile);

        foreach ($items as $name => $item) {
            $class = $item['type'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();

            $this->items[$name] = new $class([
                'name'        => $name,
                'description' => isset($item['description']) ? $item['description'] : null,
                'ruleName'    => isset($item['ruleName']) ? $item['ruleName'] : null,
                'data'        => isset($item['data']) ? $item['data'] : null,
                'createdAt'   => $itemsMtime,
                'updatedAt'   => $itemsMtime,
            ]);
        }

        foreach ($items as $name => $item) {
            if (isset($item['children'])) {
                foreach ($item['children'] as $childName) {
                    if (isset($this->items[$childName])) {
                        $this->children[$name][$childName] = $this->items[$childName];
                    }
                }
            }
        }

        foreach ($assignments as $userId => $roles) {
            foreach ($roles as $role) {
                $this->assignments[$userId][$role] = new Assignment([
                    'userId'    => $userId,
                    'roleName'  => $role,
                    'createdAt' => $assignmentsMtime,
                ]);
            }
        }

        foreach ($rules as $name => $ruleData) {
            $this->rules[$name] = unserialize($ruleData);
        }
    }
}
