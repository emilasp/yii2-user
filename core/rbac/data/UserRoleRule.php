<?php
namespace emilasp\user\core\rbac\data;

use yii\rbac\Rule;
use emilasp\user\core\rbac\PhpManager;

/**
 * Class UserRoleRule
 * @package emilasp\user\core\rbac\data
 */
class UserRoleRule extends Rule
{
    public $name = 'userRole';

    public function execute($user, $item, $params)
    {
        //check the role from table user
        if (isset(\Yii::$app->user->identity->role)) {
            $role = \Yii::$app->user->identity->role;
        } else {
            return false;
        }

        if ($item->name === PhpManager::ROLE_ADMIN) {
            return $role == PhpManager::ROLE_ADMIN;
        } elseif ($item->name === PhpManager::ROLE_COMPANY) {
            return $role == PhpManager::ROLE_ADMIN || $role == PhpManager::ROLE_COMPANY; //editor is a child of admin
        } elseif ($item->name === PhpManager::ROLE_MASTER) {
            return $role == PhpManager::ROLE_ADMIN || $role == PhpManager::ROLE_COMPANY || $role == PhpManager::ROLE_MASTER || $role == null; //user is a child of editor and admin, if we have no role defined this is also the default role
        } elseif ($item->name === PhpManager::ROLE_USER) {
            return $role == PhpManager::ROLE_ADMIN || $role == PhpManager::ROLE_COMPANY || $role == PhpManager::ROLE_MASTER || $role == PhpManager::ROLE_USER || $role == null; //user is a child of editor and admin, if we have no role defined this is also the default role
        } elseif ($item->name === PhpManager::ROLE_GUEST) {
            return $role == PhpManager::ROLE_ADMIN || $role == PhpManager::ROLE_COMPANY || $role == PhpManager::ROLE_MASTER || $role == PhpManager::ROLE_USER || $role == PhpManager::ROLE_GUEST || $role == null; //user is a child of editor and admin, if we have no role defined this is also the default role
        } else {
            return false;
        }
    }
}
