<?php
use \yii\rbac\Item;
use emilasp\site\rbac\PhpManager;

return [
    PhpManager::ROLE_GUEST => [
        'name' => 'Гость',
        'type' => Item::TYPE_ROLE,
        'description' => 'Гость',
        'bizRule' => null,
        'data' => null
    ],
    PhpManager::ROLE_USER => [
        'name' => 'Пользователь',
        'type' => Item::TYPE_ROLE,
        'description' => 'Пользователь',
        'children' => [
            PhpManager::ROLE_GUEST, // User can edit thing0
        ],
        'bizRule' => 'return !Yii::$app->user->isGuest;',
        'data' => null
    ],
    PhpManager::ROLE_MASTER => [
        'name' => 'Мастер',
        'type' => Item::TYPE_ROLE,
        'description' => 'Мастер',
        'ruleName' => 'UserRule',
        'children' => [
            PhpManager::ROLE_USER,
        ],
    ],
    PhpManager::ROLE_COMPANY => [
        'name' => 'Компания',
        'type' => Item::TYPE_ROLE,
        'description' => 'Компания',
        'ruleName' => 'UserRule',
        'children' => [
            PhpManager::ROLE_USER,
        ],
    ],
    PhpManager::ROLE_ADMIN => [
        'name' => 'Админ',
        'type' => Item::TYPE_ROLE,
        'description' => 'Админ',
        'ruleName' => 'AdminRule',
        'children' => [
            PhpManager::ROLE_MASTER,
            PhpManager::ROLE_COMPANY,
        ],
    ],
];
