<?php
namespace emilasp\user\core\widgets\UserIssueWidget;

use emilasp\core\components\base\Widget;
use emilasp\user\core\events\UserEvent;
use emilasp\user\core\models\forms\IssueMessageForm;
use emilasp\user\core\models\forms\IssueProductForm;
use emilasp\user\core\models\forms\IssueRecallForm;
use emilasp\user\core\models\UserIssue;
use yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class UserIssueWidget
 * @package emilasp\user\core\widgets\UserIssueWidget
 */
class UserIssueWidget extends Widget
{
    public $componentCart;

    public $views = [
        UserIssue::TYPE_RECALL  => 'recall',
        UserIssue::TYPE_MESSAGE => 'message',
        UserIssue::TYPE_PRODUCT => 'product',
        'final'                 => 'final',
    ];

    public $type;
    public $title;

    private $model;

    private $button;

    public function init()
    {
        $this->registerAssets();

        $this->setForm();
        $this->saveForm();
    }

    public function run()
    {
        if ($this->button) {
            echo $this->button;
        }
        $html = $this->render($this->views[$this->type], [
            'model' => $this->model,
        ]);

        return $html;
    }

    /**
     * Устанавливаем значения для формы
     */
    private function setForm()
    {
        switch ($this->type) {
            case UserIssue::TYPE_RECALL:
                $this->model       = new IssueRecallForm();
                $this->model->text = 'Recall';

                $this->button = Html::tag('a', '', [
                    'class' => 'issue-call',
                    'data-remodal-target' => 'user-issue-form-modal-1',
                    'title' => Yii::t('user', 'Recall'),
                ]);
                break;
            case UserIssue::TYPE_MESSAGE:
                $this->model = new IssueMessageForm();
                $this->button = Html::tag('a', '', [
                    'class' => 'issue-message',
                    'data-remodal-target' => 'user-issue-form-modal-2',
                    'title' => Yii::t('user', 'Send message'),
                ]);
                break;
            case UserIssue::TYPE_PRODUCT:
                $this->model = new IssueProductForm();
                break;
        }

        $this->model->type   = $this->type;
        $this->model->status = UserIssue::STATUS_ENABLED;
        $this->model->info   = json_encode([
            'url'    => Yii::$app->request->absoluteUrl,
            'params' => Yii::$app->request->queryParams,
        ]);

        if ($this->title) {
            $this->model->title = $this->title;
        }
    }


    /**
     * Если данные пришли сохраняем
     */
    private function saveForm()
    {
        if ($this->model->load(Yii::$app->request->post()) && $this->model->save()) {
            $event = UserEvent::EVENT_MESSAGE_USER;
            if ($this->type === UserIssue::TYPE_RECALL) {
                $event = UserEvent::EVENT_CALL_USER;
            }

            Yii::$app->getModule('user')->trigger($event, new UserEvent([
                'user' => Yii::$app->user->identity,
                'params' => ['issue'  => $this->model],
            ]));

            $this->model->type = 'final';
            Yii::$app->session->setFlash('success', \Yii::t('user', 'Message success'));
        }
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        UserIssueWidgetAsset::register($this->view);
    }
}
