<?php

namespace emilasp\user\core\widgets\UserIssueWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImCart
 * @package emilasp\user\core\widgets\UserIssueWidget
 */
class UserIssueWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'issue'
    ];
   /* public $js = [
        'issue'
    ];*/
}
