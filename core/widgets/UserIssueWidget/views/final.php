<?php use yii\widgets\Pjax; ?>

<?php Pjax::begin(['id' => 'user-issue-form']); ?>
    <div class="user-issue-form-container">
        <h1><?= Yii::t('user', 'Issue final') ?></h1>

    </div>

<?php
$id = 'user-issue-form-modal-' . $model->type;
$js = <<<JS
    var remodal = $('[data-remodal-id={$id}]');
    
    if (remodal.length) {
        var inst = $('[data-remodal-id={$id}]').remodal();
        inst.close();
    }
JS;

$this->registerJs($js);


?>
<?php Pjax::end(); ?>