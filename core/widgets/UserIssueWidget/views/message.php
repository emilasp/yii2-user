<?php use emilasp\core\extensions\BModal\BModal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax; ?>

<?php
$dropSelect = [
    'message' => Yii::t('site', 'Message'),
    'offer'   => Yii::t('site', 'Offer'),
    'error'   => Yii::t('site', 'Error'),
];
?>

<?php BModal::begin(['id' => 'user-issue-form-modal-' . $model->type]); ?>

<h2><?= Yii::t('user', 'Issue message') ?></h2>


<?php Pjax::begin(['id' => 'user-issue-form-' . $model->type]); ?>

<?php $form = ActiveForm::begin(['id' => 'user-issue-form-id' . $model->type, 'options' => ['data-pjax' => true]]); ?>
<div class="user-issue-form-container">

    <?= $form->field($model, 'title')->dropDownList($dropSelect)->label(false) ?>

    <?= $form->field($model, 'text')
             ->textarea(['rows' => 6, 'placeholder' => Yii::t('site', 'Text')])->label(false) ?>

    <?= $form->field($model, 'email')
             ->textInput(['maxlength' => true, 'placeholder' => Yii::t('site', 'Email')])->label(false) ?>

    <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'info')->hiddenInput()->label(false) ?>

    <div class="modal-footer">
        <div class="row">
            <div class="col-md-6">
                <div class="help-block text-left">
                    <?= Yii::t('user', 'Issue help text message') ?>
                </div>
            </div>
            <div class="col-md-6">
                <button data-remodal-action="cancel" class="btn btn-default"><?= Yii::t('site', 'Cancel') ?></button>
                <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>


<?php Pjax::end(); ?>
<?php BModal::end();
