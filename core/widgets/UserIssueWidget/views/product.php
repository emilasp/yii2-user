<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax; ?>

<?php Pjax::begin(['id' => 'user-issue-form-' . $model->type]); ?>

<?php $form = ActiveForm::begin(['id' => 'user-issue-form-id' . $model->type, 'options' => ['data-pjax' => true]]); ?>
<div class="user-issue-form-container">
    <?= $form->field($model, 'title')->textInput(['disabled' => 'disabled'])->label(false) ?>

    <?= $form->field($model, 'phone')
             ->widget(MaskedInput::className(), [
                 'mask'    => '+7 (999) 999-99-99',
                 'options' => [
                     'class'       => 'form-control',
                     'placeholder' => Yii::t('site', 'Phone'),
                 ],
             ])->label(false) ?>

    <?= $form->field($model, 'text')
             ->textarea(['rows' => 6, 'placeholder' => Yii::t('site', 'Answer')])->label(false) ?>


    <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'info')->hiddenInput()->label(false) ?>


    <div class="modal-footer">
        <div class="row">
            <div class="col-md-6">
                <div class="help-block text-left">
                    <?= Yii::t('user', 'Issue product help text message') ?>
                </div>
            </div>
            <div class="col-md-6">
                <button data-remodal-action="cancel" class="btn btn-default"><?= Yii::t('site', 'Cancel') ?></button>
                <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>

    </div>

</div>
<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>
