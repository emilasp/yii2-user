<?php use emilasp\core\extensions\BModal\BModal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax; ?>



<?php BModal::begin(['id' => 'user-issue-form-modal-' . $model->type]); ?>

<h2><?= Yii::t('user', 'Issue recall') ?></h2>


<?php Pjax::begin(['id' => 'user-issue-form-' . $model->type]); ?>

<?php $form = ActiveForm::begin(['id' => 'user-issue-form-id' . $model->type, 'options' => ['data-pjax' => true]]); ?>
<div class="user-issue-form-container">

    <?= $form->field($model, 'phone')
             ->widget(MaskedInput::className(), [
                 'mask'    => '+7 (999) 999-99-99',
                 'options' => [
                     'class'       => 'form-control',
                     'placeholder' => Yii::t('site', 'Phone'),
                 ],
             ])->label(false) ?>

    <?= $form->field($model, 'title')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'info')->hiddenInput()->label(false) ?>

    <div class="modal-footer">
        <div class="row">
            <div class="col-md-6">
                <div class="help-block text-left">
                    <?= Yii::t('user', 'Issue call help text message') ?>
                </div>
            </div>
            <div class="col-md-6">
                <button data-remodal-action="cancel" class="btn btn-default"><?= Yii::t('site', 'Cancel') ?></button>
                <?= Html::submitButton(Yii::t('user', 'Recall'), ['class' => 'btn btn-success issue-send']) ?>
            </div>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>


<?php Pjax::end(); ?>
<?php BModal::end(); ?>
