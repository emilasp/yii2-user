<?php
namespace emilasp\user\core\widgets\authSocial;

use yii;
use emilasp\core\components\base\Widget;

/**
 * Class AuthSocial
 * @package emilasp\user\core\widgets\authSocial
 */
class AuthSocial extends Widget
{
    public $baseAuthUrl = ['/user/login/auth'];
    public $popupMode = true;

    /**
     * @throws \Exception
     */
    public function run()
    {
        if (Yii::$app->getModule('user')->socialEnabled) {
            echo $this->render('auth', [
                'baseAuthUrl' => $this->baseAuthUrl,
                'popupMode' => $this->popupMode,
            ]);
        }
    }
}
