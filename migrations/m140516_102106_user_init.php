<?php
use emilasp\user\core\models\User;

/**
 * Class front_user_init
 * ./yii migrate/up --migrationPath=./vendor/emilasp/yii2-user-new/migrations/
 */
class m140516_102106_user_init extends \yii\db\Migration
{
    private $tableOptions = null;

    public function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
    }

    public function up()
    {
        $this->createTable('users_user', [
            'id'         => $this->primaryKey(11),
            'username'   => $this->string(50)->notNull(),
            'password'   => $this->string(128)->notNull(),
            'auth_key'   => $this->string(128)->notNull(),
            'role'       => $this->string(10)->notNull(),
            'status'     => $this->smallInteger(1)->notNull()->defaultValue(0),
            'email'      => $this->string(100),
            'phone'      => $this->string(10),
            'city_id'    => $this->integer(11),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        /*$this->createIndex('idx_user_email', 'users_user', 'email');
        $this->createIndex('idx_user_status', 'users_user', 'status');*/

        $this->createTable('users_service', [
            'id'      => $this->primaryKey(11),
            'service' => $this->string(28)->notNull(),
            'ids'     => $this->string(64)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'token'   => $this->string(128)->notNull(),
            'link'    => $this->string(200),
        ], $this->tableOptions);

        $this->addForeignKey('fk_service_user_id', 'users_service', 'user_id', 'users_user', 'id');

        $this->createTable('users_profile', [
            'id'        => $this->primaryKey(11),
            'user_id'   => $this->integer(11)->notNull(),
            'hash'      => $this->string(32)->notNull(),
            'name'      => $this->string(50)->defaultValue(null),
            'firstname' => $this->string(50)->defaultValue(null),
            'lastname'  => $this->string(50)->defaultValue(null),
            'rate'      => $this->float()->defaultValue(null),
            'data'      => $this->string(250)->defaultValue(null),
            'hometown'  => $this->string(128)->defaultValue(null),
            'gender'    => $this->smallInteger(1)->defaultValue(null),
            'photo'     => $this->string(128)->defaultValue(null),
            'url'       => $this->string(128)->defaultValue(null),
        ], $this->tableOptions);

        $this->addForeignKey('fk_profile_user_id', 'users_profile', 'user_id', 'users_user', 'id');

        $this->addAdmin();
    }

    public function down()
    {
        $this->dropTable('users_service');
        $this->dropTable('users_profile');
        $this->dropTable('users_user');
    }

    /**
     * Добавляем админа
     */
    private function addAdmin()
    {
        $this->insert('users_user', [
            'username' => 'admin',
            'phone'    => '9261028050',
            'email'    => 'emilasp@mail.ru',
            'password' => Yii::$app->security->generatePasswordHash('cry5yy29'),
            'auth_key' => Yii::$app->security->generateRandomString(128),
            'role'     => 'admin',
            'status'   => User::STATUS_ACTIVE,
            'created_at'  => new \yii\db\Expression('NOW()'),
            'updated_at'  => new \yii\db\Expression('NOW()'),
            'created_by'  => 1,
            'updated_by'  => 1,
        ]);

        $this->insert('users_profile', [
            'user_id'   => 1,
            'name'      => 'Admin',
            'hash'      => Yii::$app->security->generateRandomString(),
            'firstname' => '',
            'lastname'  => '(root)',
        ]);
    }
}
