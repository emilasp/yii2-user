<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160427_150946_AddTableForUserQueryTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('users_issue', [
            'id'         => $this->primaryKey(11),
            'type'       => $this->integer(1),
            'title'      => $this->string(255),
            'text'       => $this->text()->notNull(),
            'name'       => $this->string(255),
            'phone'      => $this->string(50),
            'email'      => $this->string(50),
            'info'       => $this->string(250),
            'status'     => $this->smallInteger(1)->defaultValue(0)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->createIndex('users_issue_type_idx', 'users_issue', ['type']);
        $this->createIndex('users_issue_created_idx', 'users_issue', ['created_at']);

        $this->addVarietyTypes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('users_issue');

        $this->afterMigrate();
    }


    private function addVarietyTypes()
    {
        //Variety::add('user_issue_type', 'user_issue_type_recall', 'Перезвонить', 1, 1);
        //Variety::add('user_issue_type', 'user_issue_type_message', 'Сообщение', 2, 2);
        //Variety::add('user_issue_type', 'user_issue_type_error', 'Ошибка', 3, 3);
        //Variety::add('user_issue_type', 'user_issue_type_product', 'Продукт', 4, 4);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
